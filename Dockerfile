FROM node:14.4.0-slim

WORKDIR /usr/src/app
COPY build/ /usr/src/app/
RUN npm i serve -g

ENTRYPOINT ["serve"]
CMD ["-s", "."]
EXPOSE 5000